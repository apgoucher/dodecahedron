This contains the code (`dodeca.cpp`) used to search for the solution given
in [this MathOverflow answer](https://mathoverflow.net/a/366564/39521)
along with a Python notebook for verifying the resulting solution.

A more comprehensive write-up is in [the cp4space post][1].

[1]: https://cp4space.wordpress.com/2020/07/25/rational-dodecahedron-inscribed-in-unit-sphere/

This repository additionally contains a CUDA implementation, `cudodeca.cu`.
Over the course of fifteen hours, this was able to exhaustively show there
are three primitive integer solutions with $`a, b, c, d, x, y \leq 10^4`$:

    Solution: 22, 21, 22, 54, 40, 10
    Solution: 1680, 1474, 2023, 2601, 2890, 578
    Solution: 2860, 3915, 3297, 6594, 7065, 2355

All other solutions found by the CUDA program are scalar multiples of one
of the above primitive solutions.
