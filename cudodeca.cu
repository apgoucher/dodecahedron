#include "dodeca.h"

__global__ void dodecakernel() {

    int a = (blockIdx.x & ~1) * blockDim.x + (threadIdx.x << 1) + (blockIdx.x & 1) + 1;
    int d = (blockIdx.y & ~1) * blockDim.y + (threadIdx.y << 1) + (blockIdx.y & 1) + 1;
    int N = gridDim.x * blockDim.x;

    findsols(N, d, a);

}

int main() {

    dodecakernel<<<dim3(50, 50), dim3(16, 16)>>>();
    cudaDeviceSynchronize();

}
