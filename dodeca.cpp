#include "dodeca.h"

int main() {

    int64_cu N = 200;

    for (int64_cu d = 2; d <= N; d++) {
        for (int64_cu a = 1; a <= N; a++) {
            findsols(N, d, a);
        }
    }
}
