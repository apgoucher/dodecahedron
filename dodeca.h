#pragma once

#include <stdint.h>
#include <stdio.h>
#include <cmath>

/**
 * We want our __host__ __device__ functions to always be inline,
 * as function calls on the GPU are expensive. This preprocessor
 * macro defines _HD_ as a convenient alias, and falls back on a
 * CPU-only counterpart if we compile using a non-CUDA compiler
 * such as gcc or clang.
 */

#ifdef __CUDA_ARCH__
#define _HD_ __attribute__((always_inline)) __host__ __device__ inline
#else
#define _HD_ __attribute__((always_inline)) inline
#endif

/**
 * We define our own versions of int64_t and uint64_t as these
 * do not interact well with nvcc:
 */

typedef long long int64_cu;
typedef unsigned long long uint64_cu;

/**
 * Implementation of the Euclidean algorithm for computing the
 * greatest common divisor of two positive integers. We use this
 * later in the code for reducing a rational number into lowest
 * terms.
 */
_HD_ int64_cu euclid_gcd(int64_cu a, int64_cu b) {
    int64_cu c = (a < b) ? b : a;
    int64_cu d = a ^ b ^ c;

    while (d) {
        c = c % d;
        if (c == 0) { return d; }
        d = d % c;
    }
    return c;
}

/**
 * Find all solutions with b,c <= N, where a,d <= N are fixed.
 */
_HD_ void findsols(int N, int d, int a) {

    int aa = a*a;
    int ad = a*d;

    for (int b = 1; b < d; b++) {

        // we can skip 3/16th of the loop iterations by reasoning mod 8:
        int pinc = ((b | d) & a & 1) + 1;
        int pstart = ((b ^ d) & a & 1) + 1;

        // precompute this because we can:
        int b_minus_d_squared = (b - d) * (b - d);

        for (int c = pstart; c <= N; c += pinc) {

            // The determinant ad - bc must be positive:
            if (b*c >= ad) { break; }

            // Compute the radicand in the formula for y:
            int cc = c*c;
            int64_cu yy = b_minus_d_squared + cc;
            int thing   = b_minus_d_squared - cc;
            yy = yy * yy + ((int64_cu) aa) * (aa + thing + thing);

            // Evaluate the approximate integer square-root:
            if (yy <= 0) { continue; }
            int yi = (int) std::sqrt((float) yy);
            {
                // Refine this to find the exact integer square-root:
                yi ^= (yy ^ yi) & 1; // y and y^2 must have the same parity
                int64_cu dots = (((int64_cu) yi) * yi - yy) >> 2;
                int increment = 1;
                if (dots > 0) { increment = -1; dots = -dots; }
                while (dots < 0) { yi += increment; dots += yi; yi += increment; }
                if (dots != 0) { continue; }
            }

            // ----------------
            // The following code is 'cold', i.e. seldom-visited, because we have
            // eliminated most candidate points when we checked that yy is a perfect
            // square. This gives us the liberty to use more expensive computations.
            // ----------------

            int cc_plus_dd = cc + d*d;
            int aa_plus_bb = aa + b*b;
            int xden = (ad - b*c) << 1; 

            // Compute the radicand in the formula for x. This is more difficult as
            // it can overflow an int64, so we compute both an approximate (double
            // precision) result and a reduced (modulo-2^64) result:
            int64_cu sqrand = (d - 2*b)*((int64_cu) cc_plus_dd) + d*((int64_cu) aa_plus_bb);
            double    xxd = ((double) sqrand) * ((double) sqrand) - ((double) xden) * ((double) xden) * ((double) cc_plus_dd);
            uint64_cu xxi = ((uint64_cu) sqrand) * ((uint64_cu) sqrand) - ((uint64_cu) xden) * ((uint64_cu) xden) * ((uint64_cu) cc_plus_dd);

            // repair any increase in relative error incurred by the subtraction
            // so that xxd is accurate to 52 bits of precision:
            xxd = std::round(xxd * 5.421010862427522e-20) * 1.8446744073709552e+19 + ((int64_t) xxi);

            if (xxd <= 0.0) { continue; }

            // we now square-root the radicand, resulting in an expression that
            // has approximately 50 bits of precision (sqrt is allowed to be
            // wrong by a few ULPs):
            uint64_cu xi = (uint64_cu) std::sqrt((double) xxd);

            // explore the range-2 neighbourhood to attempt to find an integer
            // solution, using a modulo-2^64 check:
            if ((xi - 2) * (xi - 2) == xxi) { xi -= 2; }
            if ((xi - 1) * (xi - 1) == xxi) { xi -= 1; }
            if ((xi + 1) * (xi + 1) == xxi) { xi += 1; }
            if ((xi + 2) * (xi + 2) == xxi) { xi += 2; }
            if (xi * xi != xxi) { continue; }

            // ensure we have the positive square-root:
            int64_cu xi_s = xi;
            if (xi_s < 0) { xi_s = -xi_s; }

            // Compute the numerator and denominator in the expressions for x and y:
            int ynum = cc_plus_dd - aa_plus_bb - yi;
            int yden = 2 * (d - b);
            int64_cu xnum = sqrand - xi_s;
            // xden has already been computed earlier;

            // Verify x and y are both positive:
            if ((xnum <= 0) || (ynum <= 0)) { continue; }

            // Reduce fractions into lowest terms:
            int xgcd = euclid_gcd(xnum, xden); xnum /= xgcd; xden /= xgcd;
            int ygcd = euclid_gcd(ynum, yden); ynum /= ygcd; yden /= ygcd;

            // Check the remaining concyclicity by evaluating the determinant
            // modulo 2^64. There could be (very rare!) false positives where
            // the actual determinant is nonzero but divisble by 2^64; we still
            // output the solution nonetheless and leave it to a later check in
            // arbitrary precision to verify it is correct.
            uint64_cu det1 = ((uint64_cu) aa_plus_bb)*xden - a*xnum;
            uint64_cu det2 = (xnum - a*((uint64_cu) xden));

            det1 *= xnum;
            det2 *= xden;
            det1 *= yden;
            det2 *= ynum;
            det1 *= yden;
            det2 *= ynum;

            if (det1 == det2) {
                printf("Solution: %d, %d, %d, %d, %lld/%d, %d/%d\n",
                        a, b, c, d, xnum, xden, ynum, yden);
            }
        }
    }
}

